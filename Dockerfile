FROM python:3.7-alpine

COPY . /app
WORKDIR /app

RUN apk add --update \
    xvfb \
    xdpyinfo \
    chromium \
    chromium-chromedriver 

COPY requirements.txt /
RUN pip install -r requirements.txt

#CMD ["python", "HoeWarmIsHetInDelft.py"]

