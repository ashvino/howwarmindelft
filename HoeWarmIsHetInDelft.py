#!/usr/bin/env python3
"""
Script to scrape actual temperature from URL: https://weerindelft.nl
"""
from selenium import webdriver
from pyvirtualdisplay import Display
import sys
import requests
import re


def check_url_response(url):
    """
    Check status code of website url == 200
    """
    try:
        response = requests.get(url)
        if response.status_code == 200:
            valid_url = True
        else:
            valid_url = False
    except Exception as err:
        print(err)
        valid_url = False

    return valid_url


def get_element(url, element_string):
    """
    Retrieve element from URL using selenium and return string
    """

    valid_url = check_url_response(url)
    if valid_url:

        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')

        driver = webdriver.Chrome(options=options)
 
        display = Display(visible=0, size=(800, 800))
        display.start()

        driver.get(url)
        element = driver.find_element_by_id(element_string).text
    else:
        print("\nURL is not valid: %s" %url)
        element = None

    return element


def HoeWarmIsHetInDelft(url):
    """
    Return float value of temperature obtained from URL
    """
    element_string = "ajaxtemp"
    web_temperature = get_element(url, element_string)

    if web_temperature is None:
        temperature = None
    else:
        temperature_found = re.search(r'\d+\.\d+', web_temperature)
        if temperature_found:
            temperature = float(temperature_found.group())

    return temperature

def main():
    main_url = "https://weerindelft.nl"
    # Referrer URL from Developer Tools Inspection
    referrer_url = "https://weerindelft.nl/WU/55ajax-dashboard-testpage.php"
    
    delft_temperature = HoeWarmIsHetInDelft(referrer_url)
    if delft_temperature is not None:
        rounded_temperature = round(delft_temperature)
        print("Temperature is %d degress celsius"%rounded_temperature)
    else:
        print("\nTemperature not obtained from URL")

if __name__ == "__main__":
    main()
